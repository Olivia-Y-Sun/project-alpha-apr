from django.urls import path
from .views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]


# from django.urls import path
# from todos.views import (
#     TodoListListView,
#     TodoListDetailView,
#     TodoListCreateView,
#     TodoListUpdateView,
#     TodoListDeleteView,
#     TodoItemCreateView,
#     TodoItemUpdateView,

# )

# # /todos

# urlpatterns = [
#     path("", TodoListListView.as_view(), name="todo_list_list"),
#     path("todo_lists/<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
#     path("todo_lists/create/", TodoListCreateView.as_view(), name="todo_list_create"),
#     path("todo_lists/<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_list_update"),
#     path("todo_lists/<int:pk>/delete", TodoListDeleteView.as_view(), name="todo_list_delete"),
#     path("todo_items/create/", TodoItemCreateView.as_view(), name="todo_item_create"),
#     path("todo_items/<int:pk>/edit", TodoItemUpdateView.as_view(), name="todo_item_update"),

# ]
