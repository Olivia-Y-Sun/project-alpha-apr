# from django.db import IntegrityError
from django.urls import reverse_lazy

# Add these for login, one for class, one for funtion
from django.contrib.auth.mixins import LoginRequiredMixin

# from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

# from django.views.decorators.http import require_http_methods

from tasks.models import Task


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "list.html"
    context_object_name = "tasks"

    def get_queryset(self):
        return Task.objects.filter(
            assignee=self.request.user
        )  # assignee:tasks -> one to many


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(self):
        return reverse_lazy(
            "show_project", args=[self.object.project.pk]
        )  # The current newly created task corresbonding project id


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "list.html"
    fields = ["is_completed"]

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")
        # return reverse_lazy("show_my_tasks", args=[self.object.project.pk])
